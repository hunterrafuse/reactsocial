let clock = new THREE.Clock();


let camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 10000),
light = new THREE.PointLight(0xFFFFFF, 2, 5000);
camera.position.set(1300, 0, 0),
scene = new THREE.Scene();

camera.lookAt(scene.position);
light.position.set(2000, 2000, 1500);
scene.add(light);

let marsGeo = new THREE.SphereGeometry (500, 32, 32),
marsMaterial = new THREE.MeshPhongMaterial(),
marsMesh = new THREE.Mesh(marsGeo, marsMaterial);
scene.add(marsMesh);

let loader = new THREE.TextureLoader();
marsMaterial.map = loader.load('images/earth/8081-earthmap4k-comp.jpg');
marsMaterial.bumpMap = loader.load('images/earth/elev_bump_4k-comp.jpg');
marsMaterial.specularMap = loader.load('images/earth/earthspec1k.jpg');
marsMaterial.specular = new THREE.Color('grey');

marsMaterial.bumpScale = 11;
marsMaterial.specular = new THREE.Color('#000000');

let renderer = new THREE.WebGLRenderer({antialiasing : true});
renderer.setSize(window.innerWidth - 40, window.innerHeight -40);
marsloc.appendChild(renderer.domElement);

// let controls = new THREE.OrbitControls(camera, renderer.domElement);
// controls.addEventListener('change', render);

function animate(){
  requestAnimationFrame(animate);
  // controls.update();
  render();
}

function render(){
   var delta = clock.getDelta();
   marsMesh.rotation.y += 0.1 * delta;
   renderer.clear();
   renderer.render(scene, camera);
}

animate();

marsloc.addEventListener('mousedown', function() {
  marsloc.style.cursor = "-moz-grabbing";
  marsloc.style.cursor = "-webkit-grabbing";
  marsloc.style.cursor = "grabbing";
})

marsloc.addEventListener('mouseup', function() {
  marsloc.style.cursor = "-moz-grab";
  marsloc.style.cursor = "-webkit-grab";
  marsloc.style.cursor = "grab";
})

window.addEventListener( 'resize', onWindowResize, false );

function onWindowResize(){
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth - 40, window.innerHeight - 40);
}
//	This function retuns a lesnflare THREE object to be .add()ed to the scene graph
function addLensFlare(x,y,z, size, overrideImage){
  var flareColor = new THREE.Color( 0xffffff );

  lensFlare = new THREE.LensFlare( overrideImage, 600, 0.0, THREE.AdditiveBlending, flareColor );

  //	we're going to be using multiple sub-lens-flare artifacts, each with a different size
  lensFlare.add( textureFlare1, 4096, 0.0, THREE.AdditiveBlending );
  lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
  lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
  lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );

  //	and run each through a function below
  lensFlare.customUpdateCallback = lensFlareUpdateCallback;

  lensFlare.position = new THREE.Vector3(x,y,z);
  lensFlare.size = size ? size : 16000 ;
  return lensFlare;
}

//	this function will operate over each lensflare artifact, moving them around the screen
function lensFlareUpdateCallback( object ) {
  var f, fl = this.lensFlares.length;
  var flare;
  var vecX = -this.positionScreen.x * 2;
  var vecY = -this.positionScreen.y * 2;
  var size = object.size ? object.size : 16000;

  var camDistance = camera.position.length();

  for( f = 0; f < fl; f ++ ) {
    flare = this.lensFlares[ f ];

    flare.x = this.positionScreen.x + vecX * flare.distance;
    flare.y = this.positionScreen.y + vecY * flare.distance;

    flare.scale = size / camDistance;
    flare.rotation = 0;
  }
}
var geometry   = new THREE.SphereGeometry(0.51, 32, 32)
var material  = new THREE.MeshPhongMaterial({
map     : new THREE.Texture('images/earth/earth_clouds.png'),
side        : THREE.DoubleSide,
opacity     : 0.8,
transparent : true,
depthWrite  : false,
})
var cloudMesh = new THREE.Mesh(geometry, material)
marsMesh.add(cloudMesh)
// create the geometry sphere
var geometry  = new THREE.SphereGeometry(90, 32, 32)
// create the material, using a texture of startfield
var material  = new THREE.MeshBasicMaterial()
material.map   = loader.load('images/starfield_alpha.png')
material.side  = THREE.BackSide
// create the mesh based on geometry and material
var mesh  = new THREE.Mesh(geometry, material);
