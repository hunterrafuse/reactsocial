
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
require('./components/Main');
import React from 'react';
import ReactDOM from 'react-dom';
import {  BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Main from './components/Main';
import LoginCard from './components/LoginCard';
import Register from './components/Register';
import Home from './components/Main';
import Forgot from './components/Forgot';
import Reset from './components/Reset';

ReactDOM.render(
	<Router>
	    <Switch>
	    <Route exact path='/' component={Main}/>
	    <Route path='/login' component={LoginCard}/>
	    <Route path='/register' component={Register}/>
	    <Route path='/forgotpassword' component={Forgot}/>
	    <Route path='/password/reset/:token' component={Reset}/>
	</Switch>
	</Router>,
    document.getElementById('main')
);
