import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Register from './Register';
import LoginCard from './LoginCard';
import PlanetEarth from './PlanetEarth';
import styles from '../../../../public/css/Main.css';

export default class Main extends Component {
    render() {
        return (
          <React.Fragment>
          <PlanetEarth />
            <div className="container">
                <div >
                    <div className="col-md-8">
                        <div>
                            <div className="card-body">
                                <LoginCard />
                                <Register />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </React.Fragment>
        );
    }
}

if (document.getElementById('main')) {
    ReactDOM.render(<Main />, document.getElementById('main'));
}
