import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';
import styles from '../../../../public/css/LoginCard.css';

export default class LoginCard extends Component {

  constructor(props) {
    super(props);
    this.state= {
      username: '',
      password: '',
    }
  }

  onSubmit(e){
    e.preventDefault();
    const {email, password} = this.state ;
    axios.post('api/login', {
      email,
      password
    })
    .then(response=> {
      this.setState({err: false});
      this.props.history.push("home") ;

    })
    .catch(error=> {
      this.refs.username.value="";
      this.refs.password.value="";
      this.setState({err: true});
    });
  }
  onChange(e){
    const {name, value} = e.target;
    this.setState({[name]: value});
  }

  render() {
        let error = this.state.err ;
        let msg = (!error) ? 'Login Successful' : 'Wrong Credentials' ;
        let name = (!error) ? 'alert alert-success' : 'alert alert-danger' ;
     return (
       <div className="container cardmaintext">
         <div className="col-md-8">
         <div className="col-md-offset-2 col-md-8 col-md-offset-2">
            {error != undefined && <div className={name} role="alert">{msg}</div>}
          </div>
            <div>React Social</div>
         </div>
         <h1>Login</h1>
    <form method="post" role="form" onSubmit= {()=>this.onSubmit.bind(this)}>
    	<input type="text" htmlFor="username" name="u" placeholder="Username" required="required" />
        <input type="password" name="p" placeholder="Password" onChange={()=>this.onChange.bind(this)} required="required" />
        <button data-dis-type="self-contained" type="submit" className="btn btn-primary btn-block btn-large">Let me in</button>
    </form>
        <div className="cardmain">
        </div>
        <div className="form-group">
          <div className="col-md-6 col-md-offset-4">
              <div className="checkbox">
                <label>
                  <input type="checkbox" name="remember" /> Remember Me
                </label>
              </div>
            </div>
          </div>
       </div>
     );
     disintegrate.init();
  }
}
ReactDOM.render(<LoginCard />, document.getElementById('main'));
