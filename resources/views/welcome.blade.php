
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>React Social</title>
        <script src="{{URL('https://cdnjs.cloudflare.com/ajax/libs/three.js/r83/three.min.js')}}" charset="utf-8"></script>
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div id="main"></div>
        <script src="{{mix('js/app.js')}}" ></script>
        <script src="{{asset('js/disintegrate.js')}}" ></script>
        <script src="{{asset('js/earth.js')}}"></script>
    </body>
</html>
